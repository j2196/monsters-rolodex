import { useState, useEffect } from "react";
import "./App.css";
import ClassList from "./components/card-list/card-list.component";
import SearchBox from "./components/search-box/search-box.component";

const App = () => {
  console.log('rendered')
  const [searchField, setSearchField] = useState("");
  const [monsters, setMonsters] = useState([]);
  const [filteredMonsters, setFilteredMonsters] = useState(monsters);
  const [title, setTitle] = useState("");

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((res) => res.json())
      .then((users) => setMonsters(users))
  }, []);

  useEffect(() => {
    const newFilteredMonsters = monsters.filter((monster) => {
      let name = monster.name.toLowerCase();
      return name.includes(searchField);
    });
    setFilteredMonsters(newFilteredMonsters)
  }, [monsters, searchField])

  const onSearchChange = (event) => {
    let searchFieldString = event.target.value.toLowerCase();
    setSearchField(searchFieldString);
  };

  const onTitleChange = (event) => {
    let titleFieldString = event.target.value.toLowerCase();
    setTitle(titleFieldString);
  };

  return (
    <div className="App">
      <h1 className="app-title">{title}</h1>
      <SearchBox
        className="monsters-search-box"
        placeholder="search monsters"
        onChangeHandler={onSearchChange}
      />
      <br/>
      <SearchBox
        className="title-search-box"
        placeholder="set title"
        onChangeHandler={onTitleChange}
      />
      <ClassList monsters={filteredMonsters} />
    </div>
  );
};

// class App extends Component {
//   constructor() {
//     super()

//     this.state = {
//       monsters: [],
//       searchField: ''
//     }
//   }

//   componentDidMount() {
//     fetch("https://jsonplaceholder.typicode.com/users")
//       .then((res) => res.json())
//       .then((users) =>
//         this.setState(() => {
//           return {
//             monsters: users,
//           }
//         })
//       )
//   }

//   onSearchChange = (event) => {
//     let searchField = event.target.value.toLowerCase();
//     this.setState({
//       searchField
//     })
//   }

//   render() {

//     const {monsters, searchField} = this.state;
//     const {onSearchChange} = this;

//     const filteredMonsters = monsters.filter((monster) => {
//       let name = monster.name.toLowerCase();
//       return name.includes(searchField);
//     })
//     return (
//       <div className="App">
//         <h1 className="app-title">Monsters Rolodex</h1>

//         <SearchBox className="search-box" placeholder="search monsters" onChangeHandler={onSearchChange}/>
//         <ClassList monsters={filteredMonsters}/>
//       </div>
//     );
//   }
// }

export default App;
