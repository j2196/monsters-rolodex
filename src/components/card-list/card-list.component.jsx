import Card from "../card/card.component";
import "./card-list.styles.css";

const ClassList = (props) => {
  const { monsters } = props;

  return (
    <div className="card-list">
      {monsters.map((monster) => {
        return <Card key={monster.id} monster={monster} />;
      })}
    </div>
  );
};

// class ClassList extends Component {
//   render() {
//     const { monsters } = this.props;
//     return (
//       <div className="card-list">
//         {monsters.map((monster) => {
//           return (
//             <Card key={monster.id} monster={monster}/>
//           );
//         })}
//       </div>
//     );
//   }
// }

export default ClassList;
